#!/usr/bin/env bash

# Exit if something fails
set -e

# Options
OUT="build"
NM="node_modules"

echo "-- Prepare build directory"
if [ -d "$OUT" ]; then
  rm -r "${OUT:?}/"
fi
mkdir -p $OUT/

echo "-- Build theme"
npm run buildTheme

echo "-- Copy fonts"
mkdir -p $OUT/fonts/lato
mkdir -p $OUT/fonts/roboto-slab
mkdir -p $OUT/fonts/source-code-pro
cp -r $NM/lato-font/fonts/{lato-heavy,lato-heavy-italic,lato-normal,lato-normal-italic} $OUT/fonts/lato/
cp -r $NM/roboto-slab-fontface-kit/fonts/Bold $OUT/fonts/roboto-slab/
cp -r $NM/source-code-pro/{EOT/SourceCodePro-Regular.eot,WOFF2/TTF/SourceCodePro-Regular.ttf.woff2,WOFF/OTF/SourceCodePro-Regular.otf.woff,OTF/SourceCodePro-Regular.otf,TTF/SourceCodePro-Regular.ttf} $OUT/fonts/source-code-pro/

echo "-- Import Reveal.js"
mkdir -p $OUT/css
npm run buildReveal
cp -r $NM/reveal.js/css/print/ $OUT/css/
mkdir -p $OUT/js
cp $NM/reveal.js/js/reveal.js $OUT/js*
mkdir -p $OUT/lib/js
cp $NM/reveal.js/lib/js/{classList.js,head.min.js,html5shiv.js} $OUT/lib/js/
cp -r $NM/reveal.js/plugin/ $OUT/

echo "-- Copy index file"
cp src/index.html $OUT/

echo "-- Copy EditorConfig file"
cp .editorconfig $OUT/

echo "-- Done!"
